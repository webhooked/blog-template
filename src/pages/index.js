import React from 'react'
import Link from 'gatsby-link'
import Layout from '../layouts/layout'
import Header from '../components/header/header'
import Footer from '../components/footer'
import { graphql } from 'gatsby'
import styled from 'styled-components'

const StyledLink = styled(Link)`
  margin: 0.6em 0 1.3em;
  font-size: 0.9em;
  text-align: left;
  font-weight: 500;
`

const BlogPost = ({ node }) => {
  return (
    <StyledLink to={'/' + node.slug}>
      <div className="post-album-item">
        <img src={node.heroImage.fluid.src} alt={node.title} />
        <div>
          <h1>{node.title}</h1>
          <p>{node.description.childMarkdownRemark.excerpt}</p>
        </div>
      </div>
    </StyledLink>
  )
}

const IndexPage = ({ data }) => (
  <div>
    <Header title="Webhooked"/>
    <Layout>
      <div className="post-album">
        {data.allContentfulBlogPost.edges.map(edge => (
          <BlogPost node={edge.node} key={edge.node.slug} />
        ))}
      </div>
    </Layout>
    <Footer />
  </div>
)

export default IndexPage

export const pageQuery = graphql`
  query pageQuery {
    allContentfulBlogPost(
      filter: { node_locale: { eq: "en-US" } }
      sort: { fields: [publishDate], order: DESC }
    ) {
      edges {
        node {
          title
          slug
          description {
            childMarkdownRemark {
              excerpt
            }
          }
          heroImage {
            fluid(maxWidth: 1920) {
              src
            }
          }
        }
      }
    }
  }
`
