import React, { Component } from 'react'
import { graphql } from 'gatsby'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import Layout from '../layouts/layout'
import Header from '../components/header/header'
import Footer from '../components/footer'
import { BLOCKS, MARKS } from '@contentful/rich-text-types'
import { documentToReactComponents } from '@contentful/rich-text-react-renderer'

const TitleWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  text-align: center;
  z-index: 3 !important;
  padding: 2rem 0 4rem 0;
`

const TitleImage = styled.img`
  width: 100%;
  object-fit: cover;
  margin-bottom: 3rem;
  height: 600px;

  @media (max-width: 1020px) {
    height: 620px;
  }

  @media (max-width: 1200px) {
    height: 620px;
  }
`

const Title = styled.h1`
  font-size: 58px;
  color: #212121;
  line-height: 1.1em;
  margin-bottom: 1rem;
  font-weight: 700;

  @media (max-width: 840px) {
    font-size: 46px;
  }

  @media (max-width: 640px) {
    font-size: 36px;
  }
`

const SubTitle = styled.p`
  margin: 0;
  padding: 0;
  line-height: 1.7em;
  color: #929292;
  font-size: 1.2em;

  @media (max-width: 840px) {
    font-size: 1em;
  }

  @media (max-width: 640px) {
    font-size: 0.9em;
  }
`
class BlogPost extends Component {
  render() {
    const {
      title,
      description,
      body,
      heroImage,
    } = this.props.data.contentfulBlogPost

    const Bold = ({ children }) => <span className="bold">{children}</span>
    const Text = ({ children }) => <p className="align-center">{children}</p>

    const options = {
      renderMark: {
        [MARKS.BOLD]: text => <Bold>{text}</Bold>,
      },
      renderNode: {
        [BLOCKS.PARAGRAPH]: (node, children) => <Text>{children}</Text>,
      },
    }

    return (
      <div>
        <Header title="Webhooked" />
        <TitleWrapper>
          <Title>{title}</Title>
          <SubTitle>{description.childMarkdownRemark.excerpt}</SubTitle>
        </TitleWrapper>
        <TitleImage src={heroImage.fluid.src} alt={title} />
        <Layout>
          <div className="blog-post">
            {documentToReactComponents(body.json, options)}
          </div>
        </Layout>
        <Footer />
      </div>
    )
  }
}

BlogPost.propTypes = {
  data: PropTypes.object.isRequired,
}

export default BlogPost

export const pageQuery = graphql`
  query blogPostQuery($slug: String!) {
    contentfulBlogPost(slug: { eq: $slug }) {
      title
      slug
      description {
        childMarkdownRemark {
          excerpt
        }
      }
      body {
        json
      }
      heroImage {
        fluid(maxWidth: 1920) {
          src
        }
      }
    }
  }
`
