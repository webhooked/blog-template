import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import styled from 'styled-components'
import './layout.css'

const Layout = styled.div`
  margin: 0 auto;
  padding: 0 6rem 0 6rem;

  @media (max-width: 1200px) {
    padding: 0 4rem 0 4rem;
  }

  @media (max-width: 1080px) {
    padding: 0 1rem 0 1rem;
  }
`

const TemplateWrapper = ({ children }) => (
  <div>
    <Helmet
      title="Webhooked"
      meta={[
        {
          name: 'description',
          content: 'A blog about full-stack web development',
        },
        {
          name: 'keywords',
          content:
            'web, webb, webhook, webhooks, webhooked, fullstack, full-stack, web development, development, developer',
        },
      ]}
    />
    <Layout>{children}</Layout>
  </div>
)

TemplateWrapper.propTypes = {
  function: PropTypes.func,
}

export default TemplateWrapper
