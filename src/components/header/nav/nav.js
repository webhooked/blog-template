import React from 'react'
import styled from 'styled-components'

const Container = styled.nav`
  margin: 0;
  padding: 0;

  ul {
    display: flex;
    align-items: center;
    list-style: none;
    padding: 0;
    margin: 0;

    li {
      margin: 0;
      text-transform: uppercase;
      font-size: 0.8rem;
      & + li {
        margin-left: 1rem;
      }
    }
  }
`

const Nav = () => (
  <Container>
    <ul>
      <li>
        <a href="https://www.jonedw.com">Website</a>
      </li>
    </ul>
  </Container>
)

export default Nav
