import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'
import styled from 'styled-components'
import Nav from './nav/nav'
import logo from '../../images/webhooked_text_logo.svg'

const Container = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  padding: 0 7rem 0 7rem;
  height: 140px;
  width: 100%;

  @media (max-width: 1200px) {
    padding: 0 5rem 0 5rem;
  }

  @media (max-width: 1080px) {
    padding: 0 2rem 0 2rem;
  }

  a {
    padding: 0;
    color: #929292;
    transition: color 0.2s ease;
    text-decoration: none;

    &:hover {
      color: #212121;
    }
  }
`
const LogoContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-start;
  width: 180px;
  height: auto;
`

const Logo = styled.img`
  margin: 0 auto;
`

const Header = () => (
  <Container>
    <Link to="/">
      <LogoContainer>
        <Logo src={logo} />
      </LogoContainer>
    </Link>
    <Nav />
  </Container>
)

Header.propTypes = {
  title: PropTypes.string.isRequired,
}

export default Header
