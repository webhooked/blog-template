import React from 'react'
import styled from 'styled-components'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  bottom: 0;
  padding: 0 2rem 0 2rem;

  @media (max-width: 640px) {
    padding: 0 1rem 0 1rem;
  }
`

const FooterWrapper = styled.div`
  margin: 0 auto;
  max-width: 100vw;
`

const FooterText = styled.p`
  text-align: center;
  font-size: 0.9em;
  line-height: 1.9em;
  color: #929292;
`

class FooterComponent extends React.Component {
  render() {
    return (
      <Container>
        <FooterWrapper>
          <FooterText>
            &copy;2019 - Coded with love by{' '}
            <a href="https://github.com/webhooked">webhooked</a>;
          </FooterText>
        </FooterWrapper>
      </Container>
    )
  }
}

export default FooterComponent
